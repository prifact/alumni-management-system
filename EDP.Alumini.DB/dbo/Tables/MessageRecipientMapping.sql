﻿CREATE TABLE [dbo].[MessageRecipientMapping] (
    [MessageMappingID] INT IDENTITY (1, 1) NOT NULL,
    [MessageId]        INT NOT NULL,
    [RecipientID]      INT NOT NULL,
    [ParentMessageID]  INT NULL,
    CONSTRAINT [PK__MessageR__DF31B11339C0667D] PRIMARY KEY CLUSTERED ([MessageMappingID] ASC),
    CONSTRAINT [FK__MessageRe__Messa__1ED998B2] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[Messages] ([MessageID]),
    CONSTRAINT [FK__MessageRe__Paren__1FCDBCEB] FOREIGN KEY ([ParentMessageID]) REFERENCES [dbo].[Messages] ([MessageID]),
    CONSTRAINT [FK__MessageRe__Recip__20C1E124] FOREIGN KEY ([RecipientID]) REFERENCES [dbo].[UserDetails] ([UserID])
);





