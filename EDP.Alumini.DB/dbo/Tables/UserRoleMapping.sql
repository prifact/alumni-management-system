﻿CREATE TABLE [dbo].[UserRoleMapping] (
    [UserRoleMappingID] INT IDENTITY (1, 1) NOT NULL,
    [UserID]            INT NOT NULL,
    [RoleID]            INT NOT NULL,
    PRIMARY KEY CLUSTERED ([UserRoleMappingID] ASC),
    FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID]),
    FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserDetails] ([UserID])
);





