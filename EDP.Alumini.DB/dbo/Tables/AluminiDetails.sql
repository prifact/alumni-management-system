﻿CREATE TABLE [dbo].[AluminiDetails] (
    [AluminiID]   INT             IDENTITY (1, 1) NOT NULL,
    [UserID]      INT             NOT NULL,
    [Batch]       NVARCHAR (32)   NOT NULL,
    [Address]     NVARCHAR (256)  NOT NULL,
    [DOB]         DATE            NOT NULL,
    [Photo]       VARBINARY (MAX) NOT NULL,
    [UpdatedDate] DATETIME        NOT NULL,
    CONSTRAINT [PK__AluminiD__5CF8E2992CDB4D23] PRIMARY KEY CLUSTERED ([AluminiID] ASC),
    CONSTRAINT [FK__AluminiDe__UserI__239E4DCF] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserDetails] ([UserID])
);







