﻿CREATE TABLE [dbo].[Messages] (
    [MessageID]      INT             IDENTITY (1, 1) NOT NULL,
    [MessageContent] NVARCHAR (2048) NOT NULL,
    [CreatedTime]    DATETIME        NOT NULL,
    [CreatedBy]      INT             NOT NULL,
    CONSTRAINT [PK__Messages__C87C037C7499E12F] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK__Messages__Create__1920BF5C] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserDetails] ([UserID])
);





